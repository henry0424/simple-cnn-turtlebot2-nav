#!/usr/bin/env python
# Python Image CNN Twist Publisher Node
# Note you'd still need the CMakeList.txt and package.xml

import rospy                      # rospy
import numpy as np                # numpy
import cv2                        # OpenCV2
from sensor_msgs.msg import Image # ROS Image message
from cv_bridge import CvBridge, CvBridgeError # ROS Image message -> OpenCV2 image converter
from geometry_msgs.msg import Twist #ROS Twist(linear and angular velocity) message
import sys 
import os

import math
import random
import time
import matplotlib.pyplot as plt

import tensorflow as tf
from keras.models import Sequential
from keras.layers import Input
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.optimizers import SGD, Adam
from keras.utils import np_utils
from keras.utils.vis_utils import model_to_dot

#Instantiate CV Bridge
bridge = CvBridge()

weight_file_name ="model_weights_20171228.h5"
filename = "output_data.npy"

output_data =[]

# NN related parameters
NP_BATCH_SIZE = 64

IMG_HEIGHT = 120
IMG_WIDTH = 160
IMG_CHANNEL = 1

ACT_METHOD = "relu"
VEL_OUTPUT_ACT_METHOD ="linear"
LOSS = "mse"  # "mse" for regression, "categorical_crossentropy" for classification
OPTIMIZER = "rmsprop"


def image_callback(img_msg):
    print("PyImageTwistSubscriber node  Received an image!")
    try:
        # Convert your ROS Image message to OpenCV2
        cv2_img = bridge.imgmsg_to_cv2(img_msg, "bgr8")
    except CvBridgeError, e:
        print(e)
    else:
        # Save your OpenCV2 image as a jpeg 
        # cv2.imwrite('camera_image.jpeg', cv2_img)
        input_img = cv2.cvtColor(cv2_img, cv2.COLOR_BGR2GRAY)


        # send input_img to the neural network
        print("Input_img.shape=")
        print(input_img.shape)
        input_img = input_img.reshape(-1, IMG_HEIGHT, IMG_WIDTH, IMG_CHANNEL)
        print("Reshaped input_img.shape=")
        print(input_img.shape)
        global model
        global graph
        with graph.as_default():
          Y_predict = model.predict(input_img)
        print("dbg: end of predict")
        # get output_cmd
        Y_output = np.array([data for data in Y_predict])
        print(Y_output.shape)
        print("Reshape output")
        Y_output = Y_output.reshape(2, -1)
        print(Y_output.shape)
        print(Y_output)
        print("Transpose output")
        Y_output = Y_output.T
        print(Y_output.shape)
        print(Y_output)

#        linear_vel = Y_output[0,0]
        if Y_output[0,0] > 0.26:
          linear_vel = 0.34
        else:
          linear_vel = Y_output[0,0]

        angular_vel = Y_output[0,1]
       
        # Display the converted image
        cv2.imshow("Image Display", cv2_img)
        # Wait 30 ms to allow image to be drawn.
        # Image won't display properly without this cv2.waitkey
        cv2.waitKey(30)
 
        print("XX_linear.x={0:2.2f}".format(linear_vel) + "  ZZ_angular.z={0:2.2f}".format(angular_vel))

        # publish to Twist
        output_twist = Twist()
        output_twist.linear.x = linear_vel
        output_twist.linear.y = 0
        output_twist.linear.z = 0

        output_twist.angular.x = 0
        output_twist.angular.y = 0 
        output_twist.angular.z = angular_vel

        global twist_pub
        twist_pub.publish(output_twist)

        output_cmd = [linear_vel, angular_vel]
        output_cmd = np.around(output_cmd, decimals=2)

        global output_data
        output_data.append([input_img, output_cmd])

        if len(output_data) % 500 == 0:
          print(len(output_data))
          np.save(filename, output_data)
          
def CNN_setup(weight_file_name):
    # build simple 2-layer convoluion network model
    print("Activation method = %s" % ACT_METHOD)
    print("Output activation method = %s (NO EFFECT ATM)" % VEL_OUTPUT_ACT_METHOD)
    print("Loss = %s" % LOSS)
    print("Optimizer = %s" % OPTIMIZER)

    #config tensorflow to grow memory usage
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)

    td_inputs = Input(shape=(IMG_HEIGHT, IMG_WIDTH, IMG_CHANNEL))
    conv2d_0_outputs = Convolution2D(32, 11, 11, border_mode='same', activation='relu') (td_inputs)
    pooling_0_outputs = MaxPooling2D(pool_size=(3, 3)) (conv2d_0_outputs)
    normalized_0_outputs = BatchNormalization() (pooling_0_outputs)

    conv2d_1_outputs = Convolution2D(64, 5, 5, activation='relu') (normalized_0_outputs)
    pooling_1_outputs = MaxPooling2D(pool_size=(2, 2)) (conv2d_1_outputs)
    normalized_1_outputs = BatchNormalization() (pooling_1_outputs)

    conv2d_2_outputs = Convolution2D(96, 3, 3, activation='relu') (normalized_1_outputs)

    conv2d_3_outputs = Convolution2D(96, 3, 3, activation='relu') (conv2d_2_outputs)

    conv2d_4_outputs = Convolution2D(64, 3, 3, activation='relu') (conv2d_3_outputs)
    pooling_4_outputs = MaxPooling2D(pool_size=(2, 2)) (conv2d_4_outputs)
    normalized_4_outputs = BatchNormalization() (pooling_4_outputs)

    flatten_5_outputs = Flatten() (normalized_4_outputs)

    dense_6_outputs = Dense(1024, activation='relu') (flatten_5_outputs)

    dense_7_outputs = Dense(1024, activation='relu') (dense_6_outputs)

    linear_vel_output = Dense(1, name='linear_vel_output') (dense_7_outputs)
    angular_vel_output = Dense(1, name='angular_vel_output') (dense_7_outputs)

    model = Model(input=td_inputs, output=[linear_vel_output, angular_vel_output])

    print("loading weights")
    model.load_weights(weight_file_name, by_name=True)

    #compile model
    print("Compiling model...")
    model._make_predict_function()
    model.compile(loss = LOSS, optimizer = OPTIMIZER)
    global graph
    graph = tf.get_default_graph()
    print("Model setup done")

    return model

def CNN_twist_publisher():
    # Initiate the node
    rospy.init_node('py_cnn_twist_publisher')
    # Setupt the subscription, camera/rb/image_raw and cmd_vel_mux/input/teleop is used in turtlebot_gazebo example
    rospy.Subscriber("camera/rgb/image_raw", Image, image_callback)
    
    global twist_pub
    twist_pub = rospy.Publisher("cmd_vel_mux/input/teleop", Twist, queue_size=5)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
    cv2.destroyWindow("Image Display")

if __name__ == '__main__':
    global model
    model = CNN_setup(weight_file_name)
    CNN_twist_publisher()
