#!/usr/bin/env python
# Python Image Subscriber Node
# This node was modified with reference to imcmahon's reply on
# http://answers.ros.org/question/210294/ros-python-save-snapshot-from-camera/
# Basic idea is convert from ROS Image -> CvBridge Converter -> OpenCV
# Note you'd still need the CMakeList.txt and package.xml
# Reference: 
#  http://answers.ros.org/question/210294/ros-python-save-snapshot-from-camera/
#  http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_gui/py_image_display/py_image_display.html

import rospy                      # rospy
import numpy as np                # numpy
import cv2                        # OpenCV2
from sensor_msgs.msg import Image # ROS Image message
from cv_bridge import CvBridge, CvBridgeError # ROS Image message -> OpenCV2 image converter
from geometry_msgs.msg import Twist #ROS Twist(linear and angular velocity) message
import sys 
import os

#Instantiate CV Bridge
bridge = CvBridge()
linear_x = 0.0
angular_z = 0.0

filename = "training_data.npy"

training_data =[]

def check_training_data_file(filename):
  if(os.path.isfile(filename)):
    print("File exist, loading previous data.")
    global training_data
    training_data = list(np.load(filename))
  else:
    print("File does NOT exist. Starting fresh.")
    training_data = []


def image_callback(img_msg):
    #print("PyImageTwistSubscriber node  Received an image!")
    try:
        # Convert your ROS Image message to OpenCV2
        cv2_img = bridge.imgmsg_to_cv2(img_msg, "bgr8")
    except CvBridgeError, e:
        print(e)
    else:
        # Print global linear_x and angular_z
        print("XX_linear.x={0:2.2f}".format(linear_x) + "  ZZ_angular.z={0:2.2f}".format(angular_z))

        # Display the converted image
        cv2.imshow("Image Display", cv2_img)
        # Wait 30 ms to allow image to be drawn.
        # Image won't display properly without this cv2.waitkey
        cv2.waitKey(30) 
        # Save your OpenCV2 image as a jpeg 
        # cv2.imwrite('camera_image.jpeg', cv2_img)
        input_img = cv2.cvtColor(cv2_img, cv2.COLOR_BGR2GRAY)
        output_cmd = [linear_x, angular_z]
        output_cmd = np.around(output_cmd, decimals=2)
        global training_data
        training_data.append([input_img, output_cmd])

        if len(training_data) % 500 == 0:
          print(len(training_data))
          np.save(filename, training_data)
          

def twist_callback(twist_msg):
     #print("PyImageTwistSubscriber node  Received a twist!")
     #print("linear.x={0:2.2f}".format(twist_msg.linear.x) + "  angular.z={0:2.2f}".format(twist_msg.angular.z))  
     global linear_x
     global angular_z
     linear_x = twist_msg.linear.x
     angular_z = twist_msg.angular.z

def image_twist_listener():
    # Initiate the node
    rospy.init_node('py_image_twist_listener')
    # Setupt the subscription, camera/rb/image_raw and cmd_vel_mux/input/teleop is used in turtlebot_gazebo example
    rospy.Subscriber("camera/rgb/image_raw", Image, image_callback)
    rospy.Subscriber("cmd_vel_mux/input/teleop", Twist, twist_callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
    cv2.destroyWindow("Image Display")

if __name__ == '__main__':
    check_training_data_file(filename)
    image_twist_listener()
