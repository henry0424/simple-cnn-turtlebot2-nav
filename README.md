Imitation learning using simple CNN for ROS turtlebot2(kobuki) to navigate with only one single RGB camera.

Dependencies:
1. Keras (2.1.2)
2. Tensorflow (python 2.7.4)
3. ROS Indigo
4. OpenCV (3.3.1)

Comoponents:
1. ros_py_image_twist_subscriber node
2. ros_py_cnn_twist_publisher node
3. Keras_cnn_turtlebot_0

Basic usage guide:
1. Use ros_py_image_twist_subscriber with turtlebot_gazebosim to gather unbalanced traing data
2. Run py_balance_training_data.py to balance the training data
3. Copy the balanced training data into Keras_cnn_turtlebot_0
4. In Keras_turtlebot_0, run keras_cnn_learn_nav_0.py to learn and output the model_weights
5. Copy the model weights into ros_py_cnn_twist_publisher/script and modify the python code to reat it
6. Run turtlebot_gazebosim and ros_py_cnn_twist_pubisher

Special notes:
1. Image dimension is preset to H120 x W160 pixels
2. Gazebosim world file should be specified during launch. This world file should be identical in both training and prediction.
3. Some Keras syntax are pre v2.1.2
