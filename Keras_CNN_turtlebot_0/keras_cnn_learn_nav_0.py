import math
import random
import time
import numpy as np
import matplotlib.pyplot as plt
import cv2
import os
import glob

from keras.models import Sequential
from keras.layers import Input
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.optimizers import SGD, Adam
from keras.utils import np_utils
from keras.utils.vis_utils import model_to_dot

# Raw training data file name
raw_training_data_file = "raw_training_data_20170814.npy"

# Number of epoch
NP_EPOCH = 300 
NP_BATCH_SIZE = 64 

IMG_HEIGHT = 120
IMG_WIDTH = 160
IMG_CHANNEL = 1

#ACT_METHOD = "sigmoid"
ACT_METHOD = "relu"
VEL_OUTPUT_ACT_METHOD ="linear"
LOSS = "mse"  # "mse" for regression, "categorical_crossentropy" for classification
OPTIMIZER = "rmsprop"

LOAD_WEIGHT = True #True 
SAVE_WEIGHT = True #True


def load_data(filename):
  print("loading data file")
  if(os.path.isfile(filename)):
    print("File exist, loading")
    training_data = np.load(filename)
  else:
    print("File does NOT exist.")
    exit()

  print("lenth of original training_data={}".format(len(training_data)))

#  training_data = training_data.astype('float32')

  train = training_data[:-500]
  test = training_data[-500:]

  print("lenth of train={}".format(len(train)))
  print("lenth of test={}".format(len(test)))

  img_train = np.array([data[0] for data in train])
  print(img_train.shape)
  img_train = img_train.reshape(-1, IMG_HEIGHT, IMG_WIDTH, IMG_CHANNEL)
  print(img_train.shape)
  cmd_train = np.array([data[1] for data in train])
  print(cmd_train.shape)
  
  img_test = np.array([data[0] for data in test])
  img_test = img_test.reshape(-1, IMG_HEIGHT, IMG_WIDTH, IMG_CHANNEL)
  cmd_test = np.array([data[1] for data in test])

  return img_train, cmd_train, img_test, cmd_test



# Load data from each label-directory
print("== Reading training and test data ==") 
(X_train, Y_train, X_test, Y_test) = load_data(raw_training_data_file)

print(X_train.shape)
print(Y_train.shape)
print(type(X_train))
print(type(Y_train))


# Draw samples of input images
#for i in range(80, 140, 1):
#    cv2.imshow("X_train[%d]" % i , X_train[i])
#    cv2.waitKey(250)
#    cv2.destroyAllWindows()

data_img_size = IMG_HEIGHT * IMG_WIDTH * IMG_CHANNEL

#Y_train = np_utils.to_categorical(Y_train, label_cnt)
#Y_test = np_utils.to_categorical(Y_test, label_cnt)

print(Y_train[0])
print(Y_train[1])

print(Y_test[1])

# build simple 2-layer convoluion network model
print("Activation method = %s" % ACT_METHOD)
print("Output activation method = %s (NO EFFECT ATM)" % VEL_OUTPUT_ACT_METHOD)
print("Loss = %s" % LOSS)
print("Optimizer = %s" % OPTIMIZER)

td_inputs = Input(shape=(IMG_HEIGHT, IMG_WIDTH, IMG_CHANNEL))
conv2d_0_outputs = Convolution2D(32, 11, 11, border_mode='same', activation='relu') (td_inputs)
pooling_0_outputs = MaxPooling2D(pool_size=(3, 3)) (conv2d_0_outputs)
normalized_0_outputs = BatchNormalization() (pooling_0_outputs)

conv2d_1_outputs = Convolution2D(64, 5, 5, activation='relu') (normalized_0_outputs)
pooling_1_outputs = MaxPooling2D(pool_size=(2, 2)) (conv2d_1_outputs)
normalized_1_outputs = BatchNormalization() (pooling_1_outputs)

conv2d_2_outputs = Convolution2D(96, 3, 3, activation='relu') (normalized_1_outputs)

conv2d_3_outputs = Convolution2D(96, 3, 3, activation='relu') (conv2d_2_outputs)

conv2d_4_outputs = Convolution2D(64, 3, 3, activation='relu') (conv2d_3_outputs)
pooling_4_outputs = MaxPooling2D(pool_size=(2, 2)) (conv2d_4_outputs)
normalized_4_outputs = BatchNormalization() (pooling_4_outputs)

flatten_5_outputs = Flatten() (normalized_4_outputs)

dense_6_outputs = Dense(1024, activation='relu') (flatten_5_outputs)

dense_7_outputs = Dense(1024, activation='relu') (dense_6_outputs)

linear_vel_output = Dense(1, name='linear_vel_output') (dense_7_outputs)
angular_vel_output = Dense(1, name='angular_vel_output') (dense_7_outputs)

model = Model(input=td_inputs, output=[linear_vel_output, angular_vel_output])

if LOAD_WEIGHT:
    print("loading weights")
    model.load_weights("model_weights.h5", by_name=True)

#compile model
model.compile(loss = LOSS, optimizer = OPTIMIZER)
print("Compiling model")

def train_and_show_result(model):
    t_start = time.time()
    training_history = model.fit(X_train, [Y_train[:,0], Y_train[:,1]],
                                 batch_size = NP_BATCH_SIZE,
                                 nb_epoch=NP_EPOCH,
                                 verbose=2)

    score_0 = model.evaluate(X_test, [Y_test[:,0], Y_test[:,1]])

    t_end = time.time()
    print(training_history.history.keys())
    print(model.metrics_names)

    print("\n--------------------")
    print("Total Testing Loss: {} ".format(score_0[0]))
    print("Linear_vel Testing Loss: {} ".format(score_0[1]))
    print("Angular_vel Testing Loss: {} ".format(score_0[2]))
    print("Time= %f second\n" % (t_end - t_start))
    return training_history

def plot_training_history(training_history):
    f, axarr = plt.subplots(2, sharex=True)
    axarr[0].set_xlabel('Epoch')
    axarr[0].set_ylabel('Training linear_vel loss')
    axarr[0].plot(list(range(1, NP_EPOCH+1)), training_history.history['linear_vel_output_loss'])
    axarr[1].set_ylabel('Training angular_vel loss')
    axarr[1].plot(list(range(1, NP_EPOCH+1)), training_history.history['angular_vel_output_loss'])
    plt.show()

print("Training")
history = train_and_show_result(model)
print("End of Training")

if SAVE_WEIGHT:
    print("Saving weights")
    model.save_weights("model_weights.h5")

plot_training_history(history)

print("Activation method = %s" % ACT_METHOD)
print("Output activation method = %s" % VEL_OUTPUT_ACT_METHOD)
print("Loss = %s" % LOSS)
print("= test end =")

